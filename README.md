#projet

Le projet est de chiffrer et déchiffrer des champs dans la base mais aussi dans hasher

ATTENTION  
LE .env DOIT ETRE REMPLI CORRECTEMENT   
les dossiers indiqués à l'intérieur doivent exister

# API

BDD : mysql (base créée par sequelize)  
ORM : sequelize  
FRAMEWORK : express  
SERVEUR : http  

## Instructions à suivre pour lancer le template.

Installer node.js version 12 mini  
Se mettre à l'emplacement du dossier de l'api  
Lancer la commande : `npm install`  
Renommer le .env.sample en .env et remplier les champs  
Lancer la commande : `npm start` qui lance automatiquement la commande `node server.js`  
L'api est lancé ^^  

## Fonctionnement de l'API

Les dossiers sont découpés en fonction de leur utilités :  
 -Le dossier models ressence tout les modèles correspond aux entités en base  
 -Le dossier middleware ressence les deux middlewares principaux : verification du jwt et de la possibilités d'inscription  
 -Le dossier controllers ressence les controllers des entités et donc les fonctions associés  
 -Le dossier routes ressence les différentes routes/endpoints qui vont ensuite appelés les fonctions des controllers  
//dépendences
const bcrypt = require("bcryptjs");
const crypto = require('crypto');
const algorithm = 'aes-256-ctr';
const secretKey = 'vOVH6sdmpNWjRRIqCc7rdxs01lwHzfr3';

// à externaliser mais pas possible la de suite
// si dans .env, impossible à lire de cette machine
const poivre = "salut les zozos";

//chargement des modèles
const db = require("../models");

const iv = crypto.randomBytes(16);
const encrypt = (text) => {
    const cipher = crypto.createCipheriv(algorithm, secretKey, iv);
    let encrypted = cipher.update(text, 'utf8', 'hex');
    encrypted += cipher.final('hex');
    return {
        iv: iv.toString('hex'),
        content: encrypted
    };
};

const decrypt = (text) => {
    const decipher = crypto.createDecipheriv(algorithm, secretKey, iv);
    let decrypted = decipher.update(text, 'hex', 'utf8');
    decrypted += decipher.final('utf8');

    return {
        iv: iv.toString('hex'),
        content: decrypted
    }
}

exports.signup = async (req, res) => {
    const {email, username, password} = req.body;
    const salt = bcrypt.genSaltSync(10);
    let hashedPassword = bcrypt.hashSync(password + poivre, salt);
    await db.user.create({email: encrypt(email).content, password: hashedPassword, username: username})
        .then(user => res.json({user, msg: 'User created succesfully'}));
}

exports.signin = async (req, res) => {
    const {username, password} = req.body;
    db.user.findOne({
        where: {
            username: username
        }
    }).then(user => {
        if (!user) {
            return res.status(404).send({message: 'User Not found.'})
        }
        const passwordIsValid = bcrypt.compareSync(
            password + poivre,
            user.password
        )
        if (!passwordIsValid) {
            return res.status(401).send({
                accessToken: null,
                message: 'Invalid Password!'
            })
        }
        res.status(200).send({
            id: user.id,
            username: username,
            email: decrypt(user.email).content
        })
    }).catch(err => {
        res.status(500).send({message: err.message})
    })
}
//export base actuelle
const seqEI = require("sequelize-import-export")
const db = require("./models")
const {Client} = require("node-scp")
const fs = require("fs")
const path = require("path")
require('dotenv').config()

//export de la base
exportDB = () => {
    let dbex = new seqEI([db.user])
    let now = new Date();
    let annee   = now.getFullYear();
    let mois    = ('0'+now.getMonth()+1).slice(-2);
    let jour    = ('0'+now.getDate()   ).slice(-2);
    let heure   = ('0'+now.getHours()  ).slice(-2);
    let minute  = ('0'+now.getMinutes()).slice(-2);
    let seconde = ('0'+now.getSeconds()).slice(-2);
    let path = process.env.EXPORT_DB_FILE || './test'
    let file = path + `${annee}${mois}${jour}_${heure}${minute}${seconde}.sequelize`
    dbex.export(file).then((path) => {
        console.log(`exported successfully to "${path}!"`);
        Client({
            host: process.env.EXPORT_HOST_IP || 'your host',
            port: process.env.EXPORT_HOST_PORT || '22',
            username: process.env.EXPORT_HOST_USER || 'username',
            password: process.env.EXPORT_HOST_PASS || 'password',
            // privateKey: fs.readFileSync('./key.pem'),
            // passphrase: 'your key passphrase',
        }).then(client => {
            client.uploadFile(path, process.env.EXPORT_HOST_PATH + file || "/workspace/" + file)
                .then(response => {
                    console.log("save uploaded successfully")
                    client.close() // remember to close connection after you finish
                })
                .catch(error => {
                    console.log("error upload :", error)
                })
        }).catch(e => console.log(e))
    }).catch(err => {
        console.log("got ERROR", err);
    })
}

//import de base en cas de redemarrage
importDB = () => {
    const getMostRecentFile = (dir) => {
        const files = orderReccentFiles(dir)
        return files.length ? files[0] : undefined
    }

    const orderReccentFiles = (dir) => {
        return fs.readdirSync(dir)
            .filter((file) => fs.lstatSync(path.join(dir, file)).isFile())
            .map((file) => ({ file, mtime: fs.lstatSync(path.join(dir, file)).mtime }))
            .sort((a, b) => b.mtime.getTime() - a.mtime.getTime())
    }
    let options = { // optional parameter
        overwrite: true // if set to true destroyes / truncates the table and then inserts the data
    }
    let dbex = new seqEI([db.user])
    let importedFile = getMostRecentFile(process.env.IMPORT_DB_FOLDER)
    if(importedFile !== undefined) {
        dbex.import(importedFile.file, options).then((path) => {
            console.log(`Imported Data Successfully.`)
        }).catch(err => {
            console.log("got ERROR", err)
        })
    } else {
        console.log("nothing to import in db")
    }

}

const dbUtility = {
    exportDB: exportDB,
    importDB: importDB
}

module.exports = dbUtility
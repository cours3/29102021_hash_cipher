// prérequis
const express = require('express')
const bodyParser = require('body-parser')
const http = require("http")
const cors = require('cors')
const path = require('path')
require('dotenv').config()
const dbUtility = require('./dbUtility.js')

//variables nécessaire
const app = express()
const port = 3000

// chargement des models
const db = require("./models")

// synchronization de la base
db.sequelize.sync()
    .then(() => {
        console.log('db sync successfull')
        dbUtility.importDB()
    })

// enabling cors request
app.use(cors())
// parse application/json
app.use(bodyParser.json())
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({
    extended: true
}))

// appel des routes
require('./routes/signup.routes')(app)

//default routes de test
app.get("/", function (req, res) {
    res.sendFile(path.join(__dirname+'/view/index.html'))
})

app.get("/signin", function (req, res) {
    res.sendFile(path.join(__dirname+'/view/signin.html'))
})

//premier export de la DB
setInterval(dbUtility.exportDB, 900000)

//lancement serveur
http.createServer(app).listen(port)
